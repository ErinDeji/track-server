const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const User = require("../models/User");

module.exports = (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) {
    return res.status(401).send({ error: "You must be logged in. " });
  } else {
    const token = authorization && authorization.split(" ")[1];

    jwt.verify(token, "MY_SECRET_KEY", async (err, payload) => {
      if (err) {
        return res.status(401).send({ error: "You must be logged in. " });
      }
      const { userId } = payload;

      const user = await User.findById(userId);
      req.user = user;
      next(); // pass the execution off to whatever request the client intended
    });
  }
};
