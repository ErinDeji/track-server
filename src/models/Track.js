//Import the mongoose module
var mongoose = require("mongoose");

//Define a schema
var Schema = mongoose.Schema;
var point = new Schema({
  timestamp: Number,
  coords: {
    latitude: Number,
    longitude: Number,
    altitude: Number,
    accuracy: Number,
    heading: Number,
    speed: Number,
  },
});

var Schema = mongoose.Schema;
var track = new Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    name: { type: String, default: "" },
    locations: [point],
  },
  { timestamps: true }
);

//Export function to create "SomeModel" model class
module.exports = mongoose.model("Track", track);
