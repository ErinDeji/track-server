const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
// to handle json incoming request

const requireAuth = require("./src/middlewares/requireAuth");

// route imports
const authRoutes = require("./src/routes/authRoutes");
const trackRoutes = require("./src/routes/trackRoutes");

const app = express();

app.use(bodyParser.json());
app.use(authRoutes);
app.use(trackRoutes);

mongoose
  .connect(
    "mongodb+srv://sa:user12345678@cluster0-eba0o.mongodb.net/trackapp?retryWrites=true&w=majority",
    {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    }
  )
  .then(() => console.log("Connected to mongo instance"))
  .catch((err) => {
    console.log(err.message);
  });

app.get("/", requireAuth, (req, res) => {
  res.send(`your email is ${req.user.email}`);
});

app.listen(3000, () => {
  console.log("listenning on port 3000");
});
